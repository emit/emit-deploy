#!/bin/bash
DIR=$1
FUNC_NAME=$2
VERSION=ci-$BITBUCKET_BRANCH-$BITBUCKET_COMMIT
export PATH="$HOME/.yarn/bin:$PATH"

(
  cd $DIR
  yarn
  chmod -R 755 node_modules/
  zip -r lambda-$VERSION.zip *.js package.json yarn.lock lib/ bin/ node_modules/ .env
  aws s3 cp lambda-$VERSION.zip s3://emit-deploy-us/$FUNC_NAME/versions/$VERSION.zip
  aws --region us-west-2 lambda update-function-code --function-name $FUNC_NAME --zip-file fileb://lambda-$VERSION.zip --publish
)
