#!/bin/bash
apt-get update && apt-get install -y python python-dev zip unzip
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py
pip install awscli

# Yarn
curl -o- -L https://yarnpkg.com/install.sh | bash -s -- --version 0.18.1
export PATH=$HOME/.yarn/bin:$PATH

# Deploy tools
curl -o deploy.zip https://bitbucket.org/emit/emit-deploy/get/master.zip?$(date +%s)
unzip deploy.zip
mv emit-emit-deploy-* deploy
