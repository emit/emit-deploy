#!/bin/bash
APPLICATION_NAME=$1
ENVIRONMENT_NAME=$2
export TAG=ci-$BITBUCKET_BRANCH-$BITBUCKET_COMMIT

aws --region us-west-2 elasticbeanstalk create-application-version \
  --application-name $APPLICATION_NAME \
  --version-label $TAG \
  --source-bundle S3Bucket=emit-deploy-us,S3Key=$APPLICATION_NAME/$TAG.zip || echo "Version $TAG already exists"

aws --region us-west-2 elasticbeanstalk update-environment \
  --application-name $APPLICATION_NAME \
  --environment-name $ENVIRONMENT_NAME \
  --version-label $TAG || exit 1

  while true; do
      status=$(aws --region us-west-2 elasticbeanstalk describe-environments --environment-names $ENVIRONMENT_NAME --query 'Environments[0].Status')
      echo "Environment status is $status"
      if [[ $status == *"Updating"* ]]; then
        sleep 5
      else
        break
      fi
  done
