#!/bin/bash
IMAGE_NAME=$1
export TAG=ci-$BITBUCKET_BRANCH-$BITBUCKET_COMMIT

# Upload EB config
sed s/\$TAG/`echo $TAG`/ Dockerrun-template.aws.json > Dockerrun.aws.json
zip -r $TAG.zip Dockerrun.aws.json .ebextensions
aws s3 cp $TAG.zip s3://emit-deploy-us/$IMAGE_NAME/$TAG.zip
