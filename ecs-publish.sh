#!/bin/bash
IMAGE_NAME=$1
export TAG=ci-$BITBUCKET_BRANCH-$BITBUCKET_COMMIT

# Get the ECR login
aws ecr get-login --region us-west-2 | bash

# Docker build
docker build -t $IMAGE_NAME .
docker tag $IMAGE_NAME:latest 261891511151.dkr.ecr.us-west-2.amazonaws.com/$IMAGE_NAME:latest
docker push 261891511151.dkr.ecr.us-west-2.amazonaws.com/$IMAGE_NAME:latest
docker tag $IMAGE_NAME:latest 261891511151.dkr.ecr.us-west-2.amazonaws.com/$IMAGE_NAME:$TAG
docker push 261891511151.dkr.ecr.us-west-2.amazonaws.com/$IMAGE_NAME:$TAG
